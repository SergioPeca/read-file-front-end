//Dependencies
import React, {Component} from 'react';

//Assets
import './upload.css';
import UploadInput from '../commons/upload_input';

class Upload extends Component {
  render() {
    return (
        <div className="Upload">
          <h1>Upload file!</h1>
          <UploadInput/>
        </div>
    );
  }
}

export default Upload;
